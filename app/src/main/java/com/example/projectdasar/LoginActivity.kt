package com.example.projectdasar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.example.projectdasar.Model.LoginModel
import com.example.projectdasar.NetworkModule.RetrofitAPIService
import com.example.projectdasar.Rest.RestApi
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Url
import java.lang.Exception

class LoginActivity : AppCompatActivity() {
    lateinit var restApi: RestApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        restApi = RetrofitAPIService.getrestapi(RetrofitAPIService.getRetrofit())

        btnMasuk.setOnClickListener {
            if(!TextUtils.isEmpty(edPassword?.text.toString())&&!TextUtils.isEmpty(edUsername?.text.toString())){
                var username=edUsername?.text.toString()
                var password =edPassword?.text.toString()
                Login("login.php?status=login&username="+username+"&password="+password)

            }else {
                Toast.makeText(this,"Username Dan Password harus diiisi",Toast.LENGTH_SHORT).show()
            }

        }
        btnDaftar.setOnClickListener { }

    }

    fun Login(url: String) {
        restApi.getlogin(url).enqueue(object : Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<LoginModel>, response: Response<LoginModel>) {
                try {
                    val response = response.body()
                    if (response != null) {
                        if (response.status.equals("200")) {
                            val intens = Intent(this@LoginActivity,MainActivity::class.java)
                            startActivity(intens)
                        } else if(response.status.equals("201")){
                            Toast.makeText(this@LoginActivity,response.message,Toast.LENGTH_SHORT).show()
                        }
                    }
                } catch (e: Exception) {
                }
            }
        })
    }
}

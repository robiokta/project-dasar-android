package com.example.projectdasar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_bottom_shit.*

class BottomShitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_shit)
        val navControll = Navigation.findNavController(this,R.id.nav_host_fragment)
        bottomNavigation!!.setupWithNavController(navControll)
        bottomNavigation!!.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                NavigationUI.onNavDestinationSelected(item,navControll)
                return true

            }
        })
    }
}

package com.example.projectdasar.Rest

import com.example.projectdasar.Model.LoginModel
import com.example.projectdasar.Model.MenuModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface RestApi{

    @GET
    fun getlogin(@Url url: String):Call<LoginModel>

    @GET
    fun datalistmenu(@Url url: String):Call<MenuModel>

}

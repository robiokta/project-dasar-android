package com.example.projectdasar.Adapter

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.projectdasar.Model.DataMenu
import com.example.projectdasar.R
import kotlinx.android.synthetic.main.rc_item_menu.view.*

class AdapterMenu (val dataList: List<DataMenu>): RecyclerView.Adapter<AdapterMenu.MyViewHolder>() {
    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {

        fun byDataMenu(data:DataMenu)= with(itemView){

        }
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       holder.byDataMenu(dataList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
      return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rc_item_menu,parent))
        }

    override fun getItemCount(): Int {
       return dataList.size?:0
    }


}
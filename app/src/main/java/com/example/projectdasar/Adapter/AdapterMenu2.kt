package com.example.projectdasar.Adapter


import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.widget.Filter
import android.widget.Filterable
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.projectdasar.Model.DataMenu
import com.example.projectdasar.R
import kotlinx.android.synthetic.main.rc_item_menu.view.*
import java.util.*

class AdapterMenu2(private val dataList: List<DataMenu>, val onclicked: onItemClickedAdapterMenu2) :
    RecyclerView.Adapter<AdapterMenu2.MyViewHolder>(), Filterable {

    var filtersearch: List<DataMenu>? = dataList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder =
        MyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.rc_item_menu,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) =
        holder.bindDataMenu(filtersearch!![position])

    override fun getItemCount() = filtersearch!!.size

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindDataMenu(data: DataMenu) = with(itemView) {

            Glide.with(context).load(data.image).into(IvGambarBuah)

            idTextlogo.text=data.nama
            detail.text=data.alamat
            setOnClickListener {
                onclicked.onAdapterMenu2Selected(data)
            }
        }
    }

    override fun getFilter(): Filter {

        return object : Filter() {
            override fun performFiltering(text_lay: CharSequence): FilterResults {
                val charString: String = text_lay.toString()

                if (charString.isEmpty()) {
                    filtersearch = dataList
                } else {
                    val searchList: ArrayList<DataMenu> = arrayListOf()
                    for (dataset: DataMenu in dataList) {
                        if (dataset.nama.toLowerCase().contains(text_lay.toString().toLowerCase())) {
                            searchList.add(dataset)
                        }
                    }

                    filtersearch = searchList
                }

                val filterResult: FilterResults = FilterResults()
                filterResult.values = filtersearch

                return filterResult
            }

            override fun publishResults(text_lay: CharSequence?, hasil: FilterResults) {
                filtersearch = hasil.values as List<DataMenu>
                notifyDataSetChanged()
            }
        }
    }

    interface onItemClickedAdapterMenu2 {
        fun onAdapterMenu2Selected(data: DataMenu)
    }
}
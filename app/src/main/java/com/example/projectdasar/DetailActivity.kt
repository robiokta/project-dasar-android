package com.example.projectdasar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.rc_item_menu.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val extra=intent.extras
        if (extra!=null){
            Glide.with(this).load(extra.getString("image")).into(IvGambar)
            labelnama.text=extra.getString("nama")
            labelalamat.text=extra.getString("alamat")

        }
    }
}

package com.example.projectdasar.NetworkModule

import com.example.projectdasar.Rest.RestApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitAPIService {
    fun getrestapi(retrofit: Retrofit): RestApi {
        return retrofit.create(RestApi::class.java)

    }

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://192.168.1.124/")
            .addConverterFactory(GsonConverterFactory.create()).build()
    }
}
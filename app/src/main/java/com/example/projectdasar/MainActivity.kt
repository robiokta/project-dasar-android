package com.example.projectdasar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projectdasar.Adapter.AdapterMenu
import com.example.projectdasar.Adapter.AdapterMenu2
import com.example.projectdasar.Model.DataMenu
import com.example.projectdasar.Model.LoginModel
import com.example.projectdasar.Model.MenuModel
import com.example.projectdasar.NetworkModule.RetrofitAPIService
import com.example.projectdasar.Rest.RestApi
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import kotlin.random.Random

class MainActivity : AppCompatActivity(), AdapterMenu2.onItemClickedAdapterMenu2 {
    lateinit var restApi: RestApi
    var adapter: AdapterMenu2? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        restApi = RetrofitAPIService.getrestapi(RetrofitAPIService.getRetrofit())
        rvMenu?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            itemAnimator = DefaultItemAnimator()
        }

        GetData("login.php?status=get_data_all")
    }

    fun GetData(url: String) {
        restApi.datalistmenu(url).enqueue(object : Callback<MenuModel> {
            override fun onFailure(call: Call<MenuModel>, t: Throwable) {

            }

            override fun onResponse(call: Call<MenuModel>, response: Response<MenuModel>) {
                try {
                    val response = response.body()
                    if (response != null) {
                        if (response.status.equals("200")) {
                            adapter = AdapterMenu2(response.data, this@MainActivity)
                            adapter!!.notifyDataSetChanged()
                            rvMenu?.adapter = adapter
                        } else if (response.status.equals("201")) {
                            Toast.makeText(this@MainActivity, response.message, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                } catch (e: Exception) {
                }
            }

        })
    }

    override fun onAdapterMenu2Selected(data: DataMenu) {
        Toast.makeText(this@MainActivity, data.nama, Toast.LENGTH_SHORT).show()
        val intents = Intent(this, DetailActivity::class.java)
        intents.apply {
            putExtra("image", data.image)
            putExtra("nama", data.nama)
            putExtra("alamat", data.alamat)
        }
        startActivity(intents)
    }
}

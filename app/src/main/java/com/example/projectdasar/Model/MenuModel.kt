package com.example.projectdasar.Model

data class MenuModel(
    val `data`: List<DataMenu>,
    val message: String,
    val status: String
)

data class DataMenu(
    val alamat: String,
    val image: String,
    val nama: String
)
package com.example.projectdasar.Model

data class LoginModel(
    val `data`: String,
    val message: String,
    val status: String
)
